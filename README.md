# Bloque la pub sur Internet

**[bloquelapub.net](https://bloquelapub.net/)** est un site web conçut par Résistance à L'agression Publicitaire et La Quadrature du Net avec l'aide de Framasoft, Le CECIL, Globenet, Ritimo, Creis-terminal et Zero Waste France. Il as pour but de proposer un argumentaire, des affiches, des support de campagne et un tutoriel pour bloquer la pub sur Internet. 

C'est un logiciel libre : vous pouvez le modifier, l'installer chez vous, le redistribuer et plus encore. Voir la [licence d'utilisation](LICENSE.md).

## Comment contribuer ?

Ce site est en cours de développement et votre aide est bienvenue ! Nous essayons, au cours de l'année, d'ajouter des informations, de mettre à jour celles qui existent déjà, et d'améliorer l'accessibilité du site. Si vous savez coder en HTML, CSS et Javascript, votre aide est la bienvenue pour éditer le code source ! 

Sinon, vous pouvez aussi suggérer des modifications, comme mettre à jour des suggestions, ou autre : la seule limite est que le site reste fidèle aux objectifs initiaux. 

Pour contribuer, vous pouvez demander un compte sur le [Gitlab de La Quadrature du Net](https://git.laquadrature.net/la-quadrature-du-net/bloque-la-pub-sur-internet). Vous pourrez ensuite lire [les tickets qui recensent les travaux en cours](https://git.laquadrature.net/la-quadrature-du-net/bloque-la-pub-sur-internet/-/issues), voir les [branches de développement](https://git.laquadrature.net/la-quadrature-du-net/bloque-la-pub-sur-internet/-/merge_requests) et si vous trouvez quelque chose qui vous semble faisable ou qui vous plaît, vous pouvez contribuer ! Sinon, n'hésitez pas à suggérer des changement [en ouvrant un nouveau ticket.](https://git.laquadrature.net/la-quadrature-du-net/bloque-la-pub-sur-internet/-/issues/new)

Vous pouvez, en cas de doute, contacter [l'équipe technique de la Quadrature du Net](https://matrix.to/#/#tech:laquadrature.net).

Vous pouvez aussi parler autour de vous de ce site ! Par exemple en le postant dans vos réseaux sociaux, ou en imprimant les affiches disponibles sur le site et les distribuer.

## Installer

Ce site est statique : il ne nécessite simplement qu'un serveur web pour être installé. Pour cela, [voir ce tutoriel.](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04-fr).

## Licence 

>    Bloque la pub sur Internet
>    Copyright (C) 2022  La Quadrature du Net
>
>    This program is free software: you can redistribute it and/or modify
>    it under the terms of the GNU Affero General Public License as published
>    by the Free Software Foundation, either version 3 of the License, or
>    (at your option) any later version.
>
>    This program is distributed in the hope that it will be useful,
>    but WITHOUT ANY WARRANTY; without even the implied warranty of
>    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>    GNU Affero General Public License for more details.
>
>    You should have received a copy of the GNU Affero General Public License
>    along with this program.  If not, see <https://www.gnu.org/licenses/>.
